import { Template } from 'meteor/templating';
import {Teams, Members, MyEvents} from '../imports/api/events.js';
import { Cookies } from 'meteor/ostrio:cookies';
import 'chart.js';

const cookies = new Cookies();

Session.set('teamName',"");
Session.set('memeberId','');

Template.members_show_page.helpers({
    pageData: function(){
        eventId = FlowRouter.getParam('eventId');
        teamId = FlowRouter.getParam('teamId');
        team = Teams.findOne({_id: teamId})
        event =  MyEvents.findOne({_id: eventId});
        return {team : team, event : event};
    },
    members: function(){
        teamId = FlowRouter.getParam('teamId');
        return Members.find({belongsTo: teamId });
    },
    user_url: function(){
        teamId = FlowRouter.getParam('teamId');
        memberId = FlowRouter.getParam("memberId")|| Session.get('memberId') || cookies.get(teamId);
        if((window.location.href).includes('member')){
            url = window.location.href ;    
        }else{
            url = window.location.href +"/member/" + memberId;
        }
        console.log('url ', url)
        return url;
    }

});

Template.members_show_page.events({
    'submit .add_member'(event){
        newMemberName = event.target.new_member.value; 
        event.target.new_member.value = "";
        teamId = FlowRouter.getParam('teamId');
        document.getElementsByClassName('add_member')[0].setAttribute('style','display:none');

        memberId = Members.insert({
          name: newMemberName,
          belongsTo: teamId,
          mappings: {Communicating:50,Evaluating:50, Leading:50, Deciding:50, Trusting:50, Disagreeing:50,Scheduling:50},
          color: getRandomColor(),
          createdAt : new Date()
        })
        cookies.set(teamId,memberId)
        Session.set('memeberId',memberId);
        FlowRouter.go('/event/' + eventId + '/team/' + teamId + '/member/' + memberId);
        return false;
      },
      'click .create_button'(){
        document.getElementsByClassName('add_member')[0].setAttribute('style','display:block');
        return false;
      },
      'click .open_event'(event){
        FlowRouter.go('/event/' + event.target.attributes.eventid.value);
      }
});

Template.map_yourself.events({
    'change .slider'(event){
        teamId = FlowRouter.getParam('teamId');
        memberId = FlowRouter.getParam("memberId") || Session.get('memberId') || cookies.get(teamId);
        valueName = event.target.name;
        valuesMap = Members.findOne({ _id: memberId}).mappings;
        valuesMap[event.target.name] = event.target.value;
        Members.update({ _id: memberId}, {$set: {mappings:valuesMap}});
        window.visualMap.data.datasets = getDataSets();
        window.visualMap.update();
    }
});

Template.map_yourself.helpers({
    valuesMap: function(){
        teamId = FlowRouter.getParam('teamId')
        memberId = FlowRouter.getParam("memberId") || Session.get('memberId') || cookies.get(teamId);
        return Members.findOne({ _id: memberId}).mappings;
    }
});

Template.team_page.helpers({
    validMemberId: function(){
        teamId = FlowRouter.getParam('teamId');
        memberId = FlowRouter.getParam("memberId") ||  Session.get('memberId') || cookies.get(teamId);
        console.log(' memberid value:', memberId,  '   cookie:', cookies.get(teamId), "   session:", Session.get('memberId'), "   url:",FlowRouter.getParam("memberId") );
        if(Members.findOne({_id:memberId}) != null){
            console.log('found member with id : ', memberId);
            // FlowRouter.go(FlowRouter.current.path + '/member/'+memberId);
            // BlazeLayout.render('team_page', {main: 'members_show_page', mappings: 'map_yourself'});
            return true;
        }else{
            console.log('DID NOT FIND member with id : ', memberId);
            return false;
        }

    }
});
Template.culture_visualization.helpers({
    teamName: function(){        
        teamId = FlowRouter.getParam('teamId'); 
        return  Teams.findOne({_id: teamId}).name;
    },
    updateGraph: function(){
        window.visualMap.data.datasets = getDataSets();
        window.visualMap.update();
    },
    checkDataRefresh: function(){
        teamId = FlowRouter.getParam('teamId');
        team = Teams.findOne({_id : teamId });
        members = Members.find({belongsTo: teamId});
        window.visualMap.data.datasets = getDataSets();
        window.visualMap.update();
        return false;
    }
});
Template.culture_visualization.events({
    'click .show_team_mappings'(event){
        element = document.getElementsByClassName('visual_mapping')[0]; 
        if( element.style.display == 'none'){
            event.target.innerHTML = "Hide Mapping Results and Instructions"
            element.setAttribute('style','display:block');    
        }else{
            event.target.innerHTML = "Show Mapping Results and Instructions"
            element.style.display = "none";    
        }
    },
    'click .go_to_form'(event){       
        window.open("https://forms.gle/2sCfRy4KRyUp24oY8");
    }

});

Template.culture_visualization.onRendered(function(){
    drawChart();
    window.visualMap.data.datasets = getDataSets();
    window.visualMap.update();        
});

function drawChart(){    
    canvas = this.$("#mapping");
    var ctx = canvas[0].getContext("2d");
    ctx.font = "20px sans";
    yTitles = ["Communicating",  "Evaluating",   "Leading", "Deciding", "Trusting",    "Disagreeing",     "Scheduling"];
    subtitles_left  = ["(Low context) ",  "(Direct) ",   "(Egalitarian) ", "(Consensual) ", "(Task-based) ",    "(Confrontational) ",     "(Linear-time) "];
    subtitles_right = ["(High context)", "(Indirect)", "(Hierarchical)", "(Top-Down)",  "(Relationship-based)", "(Avoids confrontation)","(Flexible-time)"];

    myChart = new Chart(ctx, {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            animation: false,
            responsive: true,
            tooltips:{
                titleFontSize:10,
            },
            scales: {    
                xAxes:[{
                    type: 'linear',
                    position: 'bottom',
                    ticks:{
                        min: -50,
                        max: 50    
                    },    
                    gridLines:{
                        display:true
                    }
                }],
                yAxes: [{
                    id: 'y_left',
                    position: 'left',
                    type: 'category',
                    labels: yTitles, //titles_left,
                    gridLines:{
                        drawBorder: false,
                        drawTicks:false,
                        color:'rgba(0, 0, 0, 0.5)'
                    }
                },
                {
                    id: 'y_left_sub',
                    position: 'left',
                    gridLines:{
                        display:false
                    },
                    type: 'category',
                    labels: subtitles_left
                },{
                    id: 'y_right_sub',
                    position: 'right',
                    gridLines:{
                        display:false
                    },
                    type: 'category',
                    labels: subtitles_right
                }

            ]
            }
        }
    });
    window.visualMap = myChart; 


};

function getDataSets(){
    data = [];
    teamId = FlowRouter.getParam('teamId');
    membersCollection = Members.find({belongsTo:teamId}).fetch();
    for(x in membersCollection){
        member = membersCollection[x];
        dataValues = swapVals(member.mappings);
        dataLabel = member.name;
        dataColor = member.color;
        uData = {
            data: dataValues,
            label: dataLabel,
            fill: false,
            backgroundColor:dataColor,
            borderColor: dataColor,
            pointBackgroundColor :dataColor,
            pointRadius :5,
        }
        data.push(uData)
    }
    return data;
}
function swapVals(data){
    newData = [];
    for(key in data){
        n = {}
        n["x"] = data[key]-50;
        n["y"] = key;
        newData.push(n);
    }
    return newData;
}

function getRandomColor() {
    var letters = '0123456789ABCDE';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 15)];
    }
    return color;
}
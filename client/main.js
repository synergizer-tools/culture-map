import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
import { FlowRouter } from 'meteor/kadira:flow-router';
import {MyEvents, Teams} from '../imports/api/events.js';
import { Cookies } from 'meteor/ostrio:cookies';
import '../lib/routers.js';
import './main.html';
import './event.html';
import './team.html';
import './events.js';
import './teams.js';

Template.create_event.events({
  'submit .create_new_event'(event){
    newEventName = event.target.new_event.value; 
    MyEvents.insert({
      name: newEventName,
      createdAt : new Date(),
      createdBy : 1
    })
    return false;
  }
});

Template.my_events.helpers({
  myEvents : function(){
    return MyEvents.find();
  },
});

Template.myEvent.helpers({
  teamCount: function(eventId){
    return Teams.find({belongsTo: eventId}).count();
  }

})

Template.myEvent.events({
  'click .open_event'(event){
    FlowRouter.go('/event/' + event.target.attributes.eventid.value);
  }
})
import { Template } from 'meteor/templating';
import {Teams, MyEvents, Members} from '../imports/api/events.js';

// Session.set('teamName',"");
Template.event_page.events({
    'click .show_results'(event){
        element = document.getElementsByClassName('all_teams_results')[0]
        if( element.style.visibility == 'hidden'){
            event.target.innerHTML = "Hide Mapping Results"
            element.setAttribute('style','visibility:visible');    
        }else{
            event.target.innerHTML = "Show Mapping Results"
            element.style.visibility = "hidden";    
        }
    }
});

Template.teams_show_page.helpers({
    myEvent: function(){
        eventId = FlowRouter.getParam('eventId');
        event = MyEvents.findOne({_id: eventId})
        if(event != null){
            eventName = event.name;
        }else{
            eventName = "";
        };
        return eventName ;
    },
    teams: function(){
        eventId = FlowRouter.getParam('eventId');
        return Teams.find({belongsTo: eventId });
    },
    memberCount: function(teamId){
        num = Members.find({belongsTo: teamId}).count();
        return num;
    }
});

Template.teamName.helpers({
    eventId: function(){
        return FlowRouter.getParam('eventId');
    }
});
Template.teamName.events({
    'click .open_team'(event){
        FlowRouter.go('/event/' + FlowRouter.getParam('eventId') + "/team/" + event.target.attributes.teamid.value );
    }
});

Template.teams_show_page.events({
    'submit .create_team'(event){
        newTeamName = event.target.new_team.value; 
        event.target.new_team.value = "";
        eventId = FlowRouter.getParam('eventId');
        document.getElementsByClassName('create_team')[0].setAttribute('style','display:none');
        Teams.insert({
          name: newTeamName,
          belongsTo: eventId,
          createdAt : new Date()
        })
        return false;
      } ,
      'click .create_button'(event){
         document.getElementsByClassName('create_team')[0].setAttribute('style','display:block');
         return false;
      }
});

Template.teamMapping.helpers({
    teamsData: function(){
        eventId =  FlowRouter.getParam('eventId');
        teams = Teams.find({belongsTo: eventId}).fetch();
        return teams;
    },    
    addCanvas(teamId){
            drawChart(teamId);
            window.charts[teamId].data.datasets = getDataSets(teamId);
            window.charts[teamId].update();                
            
        // }
    },

});

Template.singleChart.helpers({
    addCanvas(teamId){
            drawChart(teamId);
            window.charts[teamId].data.datasets = getDataSets(teamId);
            window.charts[teamId].update();                
            
    },
    checkDataRefresh: function(teamId){
        team = Teams.findOne({_id : teamId });

        members = Members.find({belongsTo: teamId}).fetch();
        window.charts[teamId].data.datasets = getDataSets(teamId);
        window.charts[teamId].update();                
        return false;
    }
});

Template.singleChart.onRendered(function(){
    window.charts = {}
    eventId =  FlowRouter.getParam('eventId');
    teams = Teams.find({belongsTo: eventId}).fetch();
    for(t in teams){
        teamId = teams[t]._id;
        drawChart(teamId);
        window.charts[teamId].data.datasets = getDataSets(teamId);
        window.charts[teamId].update();                
    }
});

Template.singleChart.events({
    'click .open_team'(event){
        teamId = event.target.id;
        FlowRouter.go('/event/' + FlowRouter.getParam('eventId') + "/team/" + event.target.attributes.teamid.value );

    }
});

function drawChart(canvasId){    
    canvas = this.$("#"+canvasId);
    var ctx = canvas[0].getContext("2d");
    ctx.font = "20px sans";
    yTitles = ["Communicating",  "Evaluating",   "Leading", "Deciding", "Trusting",    "Disagreeing",     "Scheduling"];
    subtitles_left  = ["(Low context) ",  "(Direct) ",   "(Egalitarian) ", "(Consensual) ", "(Task-based) ",    "(Confrontational) ",     "(Linear-time) "];
    subtitles_right = ["(High context)", "(Indirect)", "(Hierarchical)", "(Top-Down)",  "(Relationship-based)", "(Avoids confrontation)","(Flexible-time)"];

    myChart = new Chart(ctx, {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            animation: false,
            responsive: true,
            legend:{
                display:false,
            },
            tooltips:{
                titleFontSize:10,
            },
            scales: {    
                xAxes:[{
                    type: 'linear',
                    position: 'bottom',
                    ticks:{
                        min: -50,
                        max: 50    
                    },    
                    gridLines:{
                        display:true
                    }
                }],
                yAxes: [{
                    id: 'y_left',
                    position: 'left',
                    type: 'category',
                    labels: yTitles, //titles_left,
                    gridLines:{
                        drawBorder: false,
                        drawTicks:false,
                        color:'rgba(0, 0, 0, 0.5)'
                    },
                    ticks:{
                        fontSize: 10
                    }
                },
                // {
                //     id: 'y_left_sub',
                //     position: 'left',
                //     gridLines:{
                //         display:false
                //     },
                //     type: 'category',
                //     labels: subtitles_left,
                //     ticks:{
                //         fontSize: 10
                //     }
                // },{
                //     id: 'y_right_sub',
                //     position: 'right',
                //     gridLines:{
                //         display:false
                //     },
                //     type: 'category',
                //     labels: subtitles_right,
                //     ticks:{
                //         fontSize: 10
                //     }

                // }

            ]
            }
        }
    });
    window.charts[canvasId] = myChart;


};

function getDataSets(teamId){
    data = [];
    membersCollection = Members.find({belongsTo:teamId}).fetch();
    for(x in membersCollection){
        member = membersCollection[x];
        dataValues = swapVals(member.mappings);
        dataLabel = member.name;
        dataColor = member.color;
        uData = {
            data: dataValues,
            label: dataLabel,
            fill: false,
            backgroundColor:dataColor,
            borderColor: dataColor,
            pointBackgroundColor :dataColor,
            pointRadius :3,
        }
        data.push(uData)
    }
    return data;
}
function swapVals(data){
    newData = [];
    for(key in data){
        n = {}
        n["x"] = data[key]-50;
        n["y"] = key;
        newData.push(n);
    }
    return newData;
}
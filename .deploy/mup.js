module.exports = {
  servers: {
    one: {
      // TODO: set host address, username and password
      host: '51.20.98.32',
      username: 'ubuntu',
      password: 'ubuntupassword'
    }
  },

  app: {
    // here the app name and path are specified. can be left default
    name: 'cultureMap',
    path: '../',

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url if you have domain name associated or use http://IP address/
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://51.20.98.32',
      MONGO_URL: 'mongodb://localhost/cultureMap',
//      next line shows url for external mongodb server for example mongodb.com Atlas service. 
//      MONGO_URL: 'mongodb+srv://mongoUser:mongouserpassword@orion-yzrvr.mongodb.net/cultureMap?retryWrites=true&w=majority',
      PORT: 80,
      DISABLE_WEBSOCKETS : 1
      // HTTP_FORWARDED_COUNT: 1
      // MONGO_OPLOG_URL: 'mongodb://mongodb/local',
    },

    docker: {
      // the latest version of nodejs compatible with the latest meteor (on dec 2023)
      image: 'guillim/meteord:node14.18.2',
    },
  },

  mongo: {
// earliest version of mongodb compatible with the mongo driver from above nodejs version
    version: '3.6.23',
    servers: {
      one: {}
    }
  },
};

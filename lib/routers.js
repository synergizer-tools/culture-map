 import { Events } from "../imports/api/events";
 import { FlowRouter } from 'meteor/kadira:flow-router';
 import { BlazeLayout } from 'meteor/kadira:blaze-layout';
 import { Cookies } from 'meteor/ostrio:cookies';

 const cookies = new Cookies();
FlowRouter.route('/', {
    action: function(params, queryParams) {
        BlazeLayout.render('main_page', {main: 'events_show_page'});
    }
});


FlowRouter.route('/event/:eventId', {
    action: function(params, queryParams) {
        BlazeLayout.render('event_page', {main: 'teams_show_page'});
    }
});

FlowRouter.route('/event/:eventId/team/:teamId', {
    action: function(params, queryParams) {
    memberId = cookies.get(params.teamId);
    console.log('in The ROuter memberid value:', memberId,  '   cookie:', memberId , "   session:", Session.get('memberId'), "   url:",FlowRouter.getParam("memberId") );
    if(memberId){
        BlazeLayout.render('team_page', {main: 'members_show_page', mappings: 'map_yourself'});
    }else{
        BlazeLayout.render('team_page', {main: 'members_show_page'});        
    }

    },
    // triggersExit: [reloadChart]

});

function reloadChart(){
    window.visualMap.update();
}

FlowRouter.route('/event/:eventId/team/:teamId/member/:memberId', {
    action: function(params, queryParams) {
        BlazeLayout.render('team_page', {main: 'members_show_page', mappings: 'map_yourself'});
    }
});




import { Meteor } from 'meteor/meteor';
import {MyEvents, Teams} from '../imports/api/events.js';
import '../lib/routers';

Meteor.startup(() => {
  // code to run on server at startup
});

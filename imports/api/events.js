import { Mongo } from 'meteor/mongo';

export const MyEvents = new Mongo.Collection('events');
export const Teams = new Mongo.Collection('teams');
export const Members = new Mongo.Collection('members');
